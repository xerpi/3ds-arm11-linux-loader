#ifndef _DRAW_H_
#define _DRAW_H_

#include "3dstypes.h"
#include "config.h"

#define SCREEN_TOP_W  (400)
#define SCREEN_BOT_W  (320)
#define SCREEN_TOP_H  (240)
#define SCREEN_BOT_H  (240)

#define FB_TOP_SIZE	  (400*240*3)
#define FB_BOT_SIZE	  (320*240*3)

#define FB_TOP_LEFT1_FCRAM  (0x20184E60)
#define FB_TOP_LEFT2_FCRAM  (0x201CB370)
#define FB_TOP_RIGHT1_FCRAM (0x20282160)
#define FB_TOP_RIGHT2_FCRAM (0x202C8670)
#define FB_BOT_1_FCRAM      (0x202118E0)
#define FB_BOT_2_FCRAM      (0x20249CF0)

#define FB_TOP_LEFT1_VRAM  (VRAM_BASE)
#define FB_TOP_LEFT2_VRAM  (FB_TOP_LEFT1_VRAM  + FB_TOP_SIZE)
#define FB_TOP_RIGHT1_VRAM (FB_TOP_LEFT2_VRAM  + FB_TOP_SIZE)
#define FB_TOP_RIGHT2_VRAM (FB_TOP_RIGHT1_VRAM + FB_TOP_SIZE)
#define FB_BOT_1_VRAM      (FB_TOP_RIGHT2_VRAM + FB_TOP_SIZE)
#define FB_BOT_2_VRAM      (FB_BOT_1_VRAM      + FB_BOT_SIZE)

#define RED		0xFF0000
#define GREEN		0x00FF00
#define BLUE		0x0000FF
#define CYAN		0x00FFFF
#define PINK		0xFF00FF
#define YELLOW		0xFFFF00
#define BLACK		0x000000
#define GREY		0x808080
#define WHITE		0xFFFFFF
#define ORANGE		0xFF9900
#define LIGHT_GREEN	0x00CC00
#define PURPLE		0x660033

typedef enum {
	SCREEN_TOP = (1<<0),
	SCREEN_BOT = (1<<1)
} Screen;

void ClearScreen(Screen screen);
void copy_fb_to_vram(Screen screen);
void draw_plot(Screen screen, int x, int y, u32 color);
void draw_fillrect(Screen screen, int x, int y, int w, int h, u32 color);
//Return last X position
int font_draw_char(Screen screen, int x, int y, u32 color, char c);
int font_draw_string(Screen screen, int x, int y, u32 color, const char *string);
void font_draw_stringf(Screen screen, int x, int y, u32 color, const char *s, ...);

void console_putc(Screen screen, u32 color, char c);
void console_print(Screen screen, u32 color, const char *s);
void console_printf(Screen screen, u32 color, const char *s, ...);
void console_set_y(int new_y);
void console_reset();

void set_VRAM_framebuffers(int enable);

#define ERROR(s, args...) \
	do { \
		console_printf(s, RED, ## args); \
		while (1) { \
			if (BUTTON_PRESS(BUTTON_START)) reboot(); \
		} \
	} while (0);
#define DEBUG(s, args...) console_printf(s, WHITE, ## args)
#define DBGCOL(s, col, args...) console_printf(s, col, ## args)



#endif
