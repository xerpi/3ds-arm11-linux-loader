#ifndef FS_H
#define FS_H

#include "3dstypes.h"

#define OPEN_READ   (1<<0)
#define OPEN_WRITE  (1<<1)
#define OPEN_CREATE (1<<2)

Handle FileOpen(char *path, unsigned int flags);
unsigned int FileRead(Handle handle, void *read_buffer, unsigned int len, unsigned int offset);
void FileWrite(Handle handle, void *out_buffer, unsigned int len, unsigned int offset);
void FileWriteOffset(Handle handle, void *write_buf, unsigned int size, unsigned int offset);

#endif
