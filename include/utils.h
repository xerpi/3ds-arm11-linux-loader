#ifndef UTILS_H
#define UTILS_H

extern void backdoor(void (*func)(void));
extern void FlushDataCache();
extern void DrainWriteBuffer();
extern void disable_IRQ();
extern void enable_IRQ();
extern void disable_FIQ();
extern void enable_FIQ();
extern void disable_MPU();
extern void enable_MPU();

#endif
