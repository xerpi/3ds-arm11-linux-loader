#include "draw.h"
#include "libc.h"

static int cns_y = 8;
static int cns_x = 8;
int VRAM_fb = 0;
extern const u8 msx_font[];

void set_VRAM_framebuffers(int enable)
{
	VRAM_fb = enable;
}

static inline u32 get_fb_top_left1()
{
	if (VRAM_fb) return FB_TOP_LEFT1_VRAM;
	else         return FB_TOP_LEFT1_FCRAM;
}

static inline u32 get_fb_top_left2()
{
	if (VRAM_fb) return FB_TOP_LEFT2_VRAM;
	else         return FB_TOP_LEFT2_FCRAM;
}

static inline u32 get_fb_top_right1()
{
	if (VRAM_fb) return FB_TOP_RIGHT1_VRAM;
	else         return FB_TOP_RIGHT1_FCRAM;
}

static inline u32 get_fb_top_right2()
{
	if (VRAM_fb) return FB_TOP_RIGHT2_VRAM;
	else         return FB_TOP_RIGHT2_FCRAM;
}

static inline u32 get_fb_bot1()
{
	if (VRAM_fb) return FB_BOT_1_VRAM;
	else         return FB_BOT_1_FCRAM;
}

static inline u32 get_fb_bot2()
{
	if (VRAM_fb) return FB_BOT_2_VRAM;
	else         return FB_BOT_2_FCRAM;
}

static inline int get_screen_width(Screen screen)
{
	if (screen == SCREEN_TOP) return SCREEN_TOP_W;
	else return SCREEN_BOT_W;
}

static inline int get_screen_height(Screen screen)
{
	if (screen == SCREEN_TOP) return SCREEN_TOP_H;
	else return SCREEN_BOT_H;
}

void ClearScreen(Screen screen)
{
	int i;
	if (screen & SCREEN_TOP) {
		for (i = 0; i < FB_TOP_SIZE; i+=4) {
			*(int *)(get_fb_top_left1() + i) = 0;
			*(int *)(get_fb_top_left2() + i) = 0;
			*(int *)(get_fb_top_right1() + i) = 0;
			*(int *)(get_fb_top_right2() + i) = 0;
		}
	}
	if (screen & SCREEN_BOT) {
		for (i = 0; i < FB_BOT_SIZE; i+=4) {
			*(int *)(get_fb_bot1() + i) = 0;
			*(int *)(get_fb_bot2() + i) = 0;
		}
	}
}


void copy_fb_to_vram(Screen screen)
{
	if (screen & SCREEN_TOP) {
		memcpy((void *)FB_TOP_LEFT1_VRAM, (void *)FB_TOP_LEFT1_FCRAM, FB_TOP_SIZE);
		memcpy((void *)FB_TOP_LEFT2_VRAM, (void *)FB_TOP_LEFT2_FCRAM, FB_TOP_SIZE);
		memcpy((void *)FB_TOP_RIGHT1_VRAM, (void *)FB_TOP_RIGHT1_FCRAM, FB_TOP_SIZE);
		memcpy((void *)FB_TOP_RIGHT2_VRAM, (void *)FB_TOP_RIGHT2_FCRAM, FB_TOP_SIZE);
	}
	if (screen & SCREEN_BOT) {
		memcpy((void *)FB_BOT_1_VRAM, (void *)FB_BOT_1_FCRAM, FB_TOP_SIZE);
		memcpy((void *)FB_BOT_2_VRAM, (void *)FB_BOT_2_FCRAM, FB_TOP_SIZE);
	}
}

void draw_plot(Screen screen, int x, int y, u32 color)
{
	if (screen & SCREEN_TOP) {
		u8 *base = (u8*)((SCREEN_TOP_H-y-1)*3 +x*3*SCREEN_TOP_H);
		u8 *p1 = base + get_fb_top_left1();
		u8 *p2 = base + get_fb_top_left2();
		u8 *p3 = base + get_fb_top_right1();
		u8 *p4 = base + get_fb_top_right2();
		p1[0] = p2[0] = p3[0] = p4[0] = color & 0xFF;
		p1[1] = p2[1] = p3[1] =	p4[1] = (color>>8) & 0xFF;
		p1[2] = p2[2] = p3[2] =	p4[2] = (color>>16) & 0xFF;
	}
	if (screen & SCREEN_BOT) {
		u8 *base = (u8*)((SCREEN_BOT_H-y-1)*3 +x*3*SCREEN_BOT_H);
		u8 *p1 = base + get_fb_bot1();
		u8 *p2 = base + get_fb_bot2();
		p1[0] = p2[0] = color & 0xFF;
		p1[1] = p2[1] = (color>>8) & 0xFF;
		p1[2] = p2[2] = (color>>16) & 0xFF;
	}
}

void draw_fillrect(Screen screen, int x, int y, int w, int h, u32 color)
{
	int i, j;
	for (i = 0; i < w; ++i) {
		for (j = 0; j < h; ++j) {
			draw_plot(screen, x+i, y+j, color);
		}
	}
}

int font_draw_char(Screen screen, int x, int y, u32 color, char c)
{
	u8 *font = (u8*)(msx_font + (c - (u32)' ') * 8);
	int i, j;
	for (i = 0; i < 8; ++i) {
		for (j = 0; j < 8; ++j) {
			draw_plot(screen, x+j, y+i, BLACK);
			if ((*font & (128 >> j))) draw_plot(screen, x+j, y+i, color);
		}
		++font;
	}
	return x+8;
}

int font_draw_string(Screen screen, int x, int y, u32 color, const char *string)
{
	if (string == NULL) return x;
	int startx = x;
	const char *s = string;
	while (*s) {
		if (*s == '\n') {
			x = startx;
			y+=8;
		} else if (*s == ' ') {
			x+=8;
		} else if(*s == '\t') {
			x+=8*4;
		} else {
			draw_fillrect(screen, x, y, 8, 8, BLACK);
			font_draw_char(screen, x, y, color, *s);
			x+=8;
		}
		++s;
	}
	return x;
}

void font_draw_stringf(Screen screen, int x, int y, u32 color, const char *s, ...)
{
	char buf[128] __attribute((aligned(8)));
	char *dest = buf;
	va_list va;
	va_start(va, s);
	tfp_format(&dest, putcp, (char*)s, va);
	putcp(&dest, 0);
	va_end(va);
	font_draw_string(screen, x, y, color, buf);
}

void console_putc(Screen screen, u32 color, char c)
{
	int last_y = cns_y;
	if (c == '\n') {
		cns_y += 10;
		cns_x = 8;
	} else if (c == '\r'){
		cns_x = 8;
	} else if (c == '\t'){
		cns_x += 8*4;
	} else if (c >= ' ' && c <= 126) {
		font_draw_char(screen, cns_x, cns_y, color, c);
		cns_x += 8;
	}
	if (cns_x >= (get_screen_width(screen)-8)) {
		cns_y += 10;
		cns_x = 8;
	}
	if (cns_y >= (get_screen_height(screen)-8)) {
		cns_y = 8;
	}
	if (cns_y != last_y) {
		draw_fillrect(screen, 0, cns_y, get_screen_width(screen), 10, BLACK);
		if ((cns_y+10+8) < get_screen_height(screen)) {
			draw_fillrect(screen, 0, cns_y+10, get_screen_width(screen), 10, BLACK);
			if ((cns_y+10+10+8) < get_screen_height(screen)) {
				draw_fillrect(screen, 0, cns_y+20, get_screen_width(screen), 10, BLACK);
			}
		}
	}
}

void console_print(Screen screen, u32 color, const char *s)
{
	while (*s) {
		console_putc(screen, color, *s);
		s++;
	}
}

void console_printf(Screen screen, u32 color, const char *s, ...)
{
	char buf[128] __attribute((aligned(8)));
	char *dest = buf;
	va_list va;
	va_start(va, s);
	tfp_format(&dest, putcp, (char*)s, va);
	putcp(&dest, 0);
	va_end(va);
	console_print(screen, color, buf);
}

void console_set_y(int new_y)
{
	cns_y = new_y;
}

void console_reset()
{
	cns_x = 8;
	cns_y = 8;
}
