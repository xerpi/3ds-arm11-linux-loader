#include "hid.h"

u32 InputWait()
{
	u32 pad_state_old = HID_STATE;
	while (1) {
		u32 pad_state = HID_STATE;
		if (pad_state ^ pad_state_old)
			return ~pad_state;
		}
	}
