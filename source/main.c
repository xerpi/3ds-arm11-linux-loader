#include "3dstypes.h"
#include "draw.h"
#include "libc.h"
#include "utils.h"
#include "hid.h"
#include "FS.h"
#include "i2c.h"
#include "atag.h"
#include "config.h"

static void reboot();

extern void *arm11_stage0_start;
extern void *arm11_stage0_end;
extern void *arm11_stage1_start;
extern void *arm11_stage1_end;
#define ARM_BRANCH(cur,dst) ((0b1110<<28) | (0b101<<25) | (0b0<<24) | (((dst)-((cur)+8))>>2))

#define	MAGIC_ADDR         ((volatile void*)0x1FFFFFF8)
#define	MAGIC2_ADDR        ((volatile void*)0x1FFFFFFC)
#define	MAGIC3_ADDR        ((volatile void*)0x1FFFFFF4)
#define	MAGIC_SYNC_ADDR    ((volatile void*)0x1FFFFFF0)
#define MAGIC_INT          (*(volatile int*)MAGIC_ADDR)
#define MAGIC2_INT         (*(volatile int*)MAGIC2_ADDR)
#define MAGIC3_INT         (*(volatile int*)MAGIC3_ADDR)
#define MAGIC_SYNC_INT     (*(volatile int*)MAGIC_SYNC_ADDR)

/* We are using Device Trees now!
static void setup_atags(void *address)
{
	setup_core_tag(address, 4*1024); // 4KB pagesize
	//setup_mem_tag(VRAM_BASE, VRAM_SIZE);
	//setup_mem_tag(AXI_WRAM_BASE, AXI_WRAM_SIZE);
	setup_mem_tag(FCRAM_BASE, FCRAM_SIZE);
	setup_cmdline_tag("");
	setup_end_tag();
}*/

// Linux console
static int shared_block = 1;
static unsigned int cur_color = WHITE;

#define KERN_SOH_ASCII  '\001'
static unsigned int get_kern_level_color(char kern)
{
	switch (kern) {
	case '0':  // KERN_EMERG
		return RED;
	case '1': // KERN_ALERT
		return CYAN;
	case '2': // KERN_CRIT
		return ORANGE;
	case '3': // KERN_ERR
		return PINK;
	case '4': // KERN_WARNING
		return YELLOW;
	case '5': // KERN_NOTICE
		return LIGHT_GREEN;
	case '6': // KERN_INFO
		return GREEN;
	case '7': // KERN_DEBUG
		return GREY;
	case 'd': // KERN_DEFAULT
	default:
		return WHITE;
	}
}

static char shared_getc()
{
	register char c;

	while ((c = SHARED_CHAR) == 0) {
		if (BUTTON_PRESS(BUTTON_START)) {
			reboot();
		}
	}

	do {
		if (BUTTON_PRESS(BUTTON_START)) {
			reboot();
		} else if (BUTTON_PRESS(BUTTON_R1)) {
			shared_block = 1;
		} else if (BUTTON_PRESS(BUTTON_L1)) {
			shared_block = 0;
		}
	} while (!BUTTON_PRESS(BUTTON_Y) && shared_block);

	SHARED_CHAR = 0;

	return c;
}


void pwn_ARM11()
{
	DEBUG(SCREEN_BOT, "Flushing the dcache...\n");
	FlushDataCache();
	DrainWriteBuffer();

	disable_IRQ();
	disable_FIQ();
	disable_MPU();

	// Enable r/w to VRAM
	// And switch to VRAM FB
	asm volatile (
		//Drain write buffer
		"mcr p15, 0, r0, c7, c10, 4\n"

		//Map VRAM to region 7
		//Region base: 0x18000000
		//Region size: 8MB (0b10110)
		"ldr r0, =0x1800002D\n"
		"mcr p15, 0, r0, c6, c7, 0\n"

		//Set region 7 permissions:
		//Privileged: Read/write access
		//User: Read/write access
		"mrc p15, 0, r0, c5, c0, 2\n"
		"bic r0, r0, #(0b1111 << 28)\n"
		"orr r0, r0, #(0b0011 << 28)\n"
		"mcr p15, 0, r0, c5, c0, 2\n"

		: : : "r0"
	);

	copy_fb_to_vram(SCREEN_BOT);
	set_VRAM_framebuffers(1);
	ClearScreen(SCREEN_TOP);
	SHARED_CHAR = 0;

	DBGCOL(SCREEN_BOT, PINK, "Copying ARM11 payloads...\n\n");

	const unsigned int arm11_stage0_size = (u32)&arm11_stage0_end - (u32)&arm11_stage0_start;
	const unsigned int arm11_stage1_size = (u32)&arm11_stage1_end - (u32)&arm11_stage1_start;

	memcpy((void *)ARM11_STAGE0_ADDR, &arm11_stage0_start, arm11_stage0_size);
	memcpy((void *)ARM11_STAGE1_ADDR, &arm11_stage1_start, arm11_stage1_size);

	//Overwrite ARM11 exception vectors with a jump to the ARM11 stage0 payload
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 0)  = ARM_BRANCH(ARM11_EXCVEC_ADDR_VA+0,  ARM11_STAGE0_ADDR_VA);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 4)  = ARM_BRANCH(ARM11_EXCVEC_ADDR_VA+4,  ARM11_STAGE0_ADDR_VA);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 8)  = ARM_BRANCH(ARM11_EXCVEC_ADDR_VA+8,  ARM11_STAGE0_ADDR_VA);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 12) = ARM_BRANCH(ARM11_EXCVEC_ADDR_VA+12, ARM11_STAGE0_ADDR_VA);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 16) = ARM_BRANCH(ARM11_EXCVEC_ADDR_VA+16, ARM11_STAGE0_ADDR_VA);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 20) = ARM_BRANCH(ARM11_EXCVEC_ADDR_VA+20, ARM11_STAGE0_ADDR_VA);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 24) = ARM_BRANCH(ARM11_EXCVEC_ADDR_VA+24, ARM11_STAGE0_ADDR_VA);
	*(volatile u32*)(ARM11_EXCVEC_ADDR + 28) = ARM_BRANCH(ARM11_EXCVEC_ADDR_VA+28, ARM11_STAGE0_ADDR_VA);

	DBGCOL(SCREEN_BOT, YELLOW, "Linux should boot anytime from now...\n");
	console_set_y(8*2);

	while (1) {
		register char c = shared_getc();
		if (c == KERN_SOH_ASCII) {
			char level = shared_getc();
			//console_printf(SCREEN_TOP, WHITE, "level: %d\n", level);
			cur_color = get_kern_level_color(level);
		} else {
			console_putc(SCREEN_TOP, cur_color, c);
		}
	}
}

int main()
{
	set_VRAM_framebuffers(0);
	ClearScreen(SCREEN_TOP | SCREEN_BOT);
	DBGCOL(SCREEN_BOT, CYAN, "ARM11 linux loader by xerpi\n\n");

	DEBUG(SCREEN_BOT, "Opening the linux image...\n");
	Handle fd = FileOpen(LINUXIMAGE_FILENAME, OPEN_READ);

	DEBUG(SCREEN_BOT, "FileOpen returned: %d\n", fd);

	if (fd == 0) {
		ERROR(SCREEN_BOT,
			"Error opening the Linux zImage\n"
			"Press START to reboot.");
	}

	//Load the kernel image code to ZIMAGE_ADDR
	unsigned int n = 0, bin_size;
	bin_size = 0;
	while ((n = FileRead(fd, (void*)((u32)ZIMAGE_ADDR+bin_size), 0x100000, bin_size)) > 0) {
		bin_size += n;
	}

	DEBUG(SCREEN_BOT, "Loaded kernel:\n");
	DEBUG(SCREEN_BOT, "    address: %p,\n", ZIMAGE_ADDR);
	DEBUG(SCREEN_BOT, "    size:    0x%08X bytes\n", bin_size);


	//Load the device tree to PARAMS_ADDR
	DEBUG(SCREEN_BOT, "\nOpening " DTB_FILENAME "\n");
	fd = FileOpen(DTB_FILENAME, OPEN_READ);

	DEBUG(SCREEN_BOT, "FileOpen returned: %d\n", fd);

	if (fd == 0) {
		ERROR(SCREEN_BOT,
			"Error opening " DTB_FILENAME "\n"
			"Press START to reboot.");
	}
	n = 0;
	bin_size = 0;
	while ((n = FileRead(fd, (void*)((u32)PARAMS_ADDR+bin_size), 0x1000, bin_size)) > 0) {
		bin_size += n;
	}

	DEBUG(SCREEN_BOT, "Loaded " DTB_FILENAME ":\n");
	DEBUG(SCREEN_BOT, "    address: %p,\n", PARAMS_ADDR);
	DEBUG(SCREEN_BOT, "    size:    0x%08X bytes\n", bin_size);

	DBGCOL(SCREEN_BOT, GREEN, "\nPress A to jump to the kernel!\n");

	while (!(InputWait() & BUTTON_A)) ;

	DEBUG(SCREEN_BOT, "Pwning the ARM11...\n");
	backdoor(pwn_ARM11);
	//We shouldn't get past this point...

	return 0;
}


void reboot()
{
    i2cWriteRegister(I2C_DEV_MCU, 0x20, 1 << 2);
    while (1) ;
}
